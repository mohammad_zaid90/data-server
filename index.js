
global.logger = console;

const express = require('express'),
    bodyParser = require('body-parser'),
    config = require('config');

//initilize express 
app = express();

// Uncaughts handlers
process.on('uncaughtException', err => {
    logger.error('Uncaught exception', err, err.stack);
    process.exit(2);
});
process.on('unhandledRejection', (reason, badPromise) => {
    logger.error(`Unhandled rejection at ${reason.stack}`);
    process.exit(3);
});

// parse application/json
app.use(bodyParser.json());

// Error handler
app.use((err, req, res, next) => {
    const returnedError = err || 'Unspecified error!';

    // Add error message to response so we can log it later
    res.errorMsg = returnedError;

    // We shouldn't get here
    if (!err) {
        return res.status(500).send('Unspecified error!')
    }

    return res.status(err.status || 400).send(err.toString() || err);
});

let countRequests = 1;

app.get('/data', (req, res) => {
    const statusCode = ((countRequests++) % 4 === 0) ? 401 : 200;
    res.sendStatus(statusCode);
});

try {
    app.listen(config.port);
    logger.info(`Intel file system is now up and listen to port ${config.port}`);

} catch (err) {
    logger.error(`Failed while start the system. ${err}`);
    process.exit(1);
}